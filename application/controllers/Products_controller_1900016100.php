<?php 

class Products_controller_1900016100 extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies
        $this->load->model('product_1900016100');
    }

    public function getProduct(){
        $data['getPoduct'] = $this->product_1900016100->getProducts();
        $this->load->view('client', $data);
    }

    public function postProduct(){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');

        $data = array(
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
         );
         $this->product_1900016100->addProduct($data);
    }

    public function deleteProduct( $id_product = NULL ){
        $data = array('id_product' => $id_product );
        $this->product_1900016100->deleteProduct($data);
    }

    public function putProduct( $id_product = NULL ){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');

        $data = array(
            'id_product' => $id_product,
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
         );
         $this->product_1900016100->updateProduct($data);
    }
}


?>