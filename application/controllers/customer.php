<?php
class Customer extends CI_Controller{

   
   public function __construct()
   {
      parent::__construct();
      $this->load->model('m_product');
      
   }
   

   public function index(){
    $data ['judul']= 'halaman home';
       $this->load->view('customer/header',$data);
       $this->load->view('customer/profile',$data);
       $this->load->view('customer/footer');
   }

   public function product(){
      $data ['judul']= 'halaman product';
      $data['product'] = $this->m_product->get_shop();
         $this->load->view('customer/header',$data);
         $this->load->view('customer/product',$data);
         $this->load->view('customer/footer');
     }

     public function contact(){
      $data ['judul']= 'halaman contact';
         $this->load->view('customer/header',$data);
         $this->load->view('customer/contact',$data);
         $this->load->view('customer/footer');
     }

     public function keranjang(){
      $data ['judul']= 'halaman keranjang';
         $this->load->view('customer/header',$data);
         $this->load->view('customer/keranjang',$data);
         $this->load->view('customer/footer');
     }
    }