<?php

class Keranjang extends CI_Controller{

    public function __construct(){
        parent::__construct();

    }

    public function index()
    {
        $this->load->view('customer/header');
        $this->load->view('customer/keranjang');
        $this->load->view('customer/footer');
    }



    public function tambah_ke_keranjang()
    {
        $redirect_page=$this->input->post('redirect_page');


        $data = array(
            'id'      => $this->input->post('id'),
            'qty'     => $this->input->post('qty'),
            'price'   => $this->input->post('price'),
            'name'    =>$this->input->post('name'),
    );
    
    $this->cart->insert($data);
    redirect($redirect_page,'refresh');
    }

    public function hapus_keranjang()
    {
        $this->cart->destroy();
        redirect('keranjang');
    }

    public function pembayaran()
    {
        $this->load->view('customer/header');
        $this->load->view('customer/pembayaran');
        $this->load->view('customer/footer');
    }

    public function proses_pesanan()
    {
        $this->cart->destroy();
        $this->load->view('customer/header');
        $this->load->view('customer/proses_pesanan');
        $this->load->view('customer/footer');
    }
}