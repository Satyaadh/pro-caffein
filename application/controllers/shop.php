<?php
class Shop extends CI_Controller{

   
   public function __construct()
   {
      parent::__construct();
      $this->load->model("m_shop");
   }
   public function index(){
       $data['judul'] = "Halaman Shop";
       $data['barang'] = $this->m_shop->get_shop();
       $this->load->view('admin/header',$data);
       $this->load->view('admin/admin_shop',$data);
       $this->load->view('admin/sidenav',$data);
       $this->load->view('admin/footer');

   }
       
    // Add a new item
    public function add()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required', array(
            'required' => 'Masukkan Nama !'
        ));
        $this->form_validation->set_rules('harga', 'Harga', 'required', array(
            'required' => 'Masukkan harga !'
        ));

        if ($this->form_validation->run() == TRUE) {
            $config['upload_path'] = './assets1/gambar/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|ico';
            $config['max_size']     = '10000';

            $this->upload->initialize($config);
            $g = "gambar";

            if (!$this->upload->do_upload($g)) {
                $data = array (
                    //'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
                    'judul'=> 'Halaman Administrator',
                    'title'=> 'Tambah Shop',
                   // 'type'=> $this->m_type->get_type(),
                   // 'genre'=> $this->m_genre->get_genre(),
                    'error_up'=> $this->upload->display_errors(),

                );
                $this->load->view('admin/header',$data);
                $this->load->view('admin/admin_shop_add',$data);
                $this->load->view('admin/sidenav',$data);
                $this->load->view('admin/footer');
            } else {
                $up_gambar = array('uploads' => $this->upload->data());
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets1/gambar/'. $up_gambar['uploads']['file_name'];
                $this->load->library('image_lib', $config);
                $data = array(
                    'nama' => $this->input->post('nama'),
                    'harga' => $this->input->post('harga'),
                    'gambar' => $up_gambar['uploads']['file_name'],
                );
                $this->m_shop->add($data);
                $this->session->set_flashdata('message', 'Film Berhasil Di Tambahkan');
                redirect('shop');
            }
        } else {
           // $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
            $data ['judul']= 'Halaman Administrator';
           // $data['type'] = $this->m_type->get_type();
           // $data['genre'] = $this->m_genre->get_genre();

            $this->load->view('admin/header',$data);
            $this->load->view('admin/admin_shop_add',$data);
            $this->load->view('admin/sidenav',$data);
            $this->load->view('admin/footer');
        }
    }

    //Update one item
    public function edit( $id_barang = NULL )
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required', array(
            'required' => 'Masukkan Nama !'
        ));
        $this->form_validation->set_rules('harga', 'Harga', 'required', array(
            'required' => 'Masukkan harga !'
        ));

        if ($this->form_validation->run() == TRUE) {
            $config['upload_path'] = './assets1/gambar/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|ico';
            $config['max_size']     = '10000';

            $this->upload->initialize($config);
            $g = "gambar";

            if (!$this->upload->do_upload($g)) {
                $data = array (
                    //'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
                    'judul'=> 'Halaman Administrator',
                    'title'=> 'Tambah shop',
                    'shop' => $this->m_shop->get_data($id_barang),
                    //'type'=> $this->m_type->get_type(),
                    //'genre'=> $this->m_genre->get_genre(),
                    'error_up'=> $this->upload->display_errors(),

                );

                $this->load->view('admin/header',$data);
                $this->load->view('admin/admin_shop_edit',$data);
                $this->load->view('admin/sidenav',$data);
                $this->load->view('admin/footer');
            } else {
                //hapus gambar
                $barang = $this->m_shop->get_data($id_barang);
                if ($barang->gambar != "") {
                    unlink('./assets1/gambar/'. $barang->gambar);
                }

                $up_gambar = array('uploads' => $this->upload->data());
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets1/gambar/'. $up_gambar['uploads']['file_name'];
                $this->load->library('image_lib', $config);
                $data = array(
                    'id_barang' => $id_barang,
                    'nama' => $this->input->post('nama'),
                    'harga' => $this->input->post('harga'),
                    'gambar' => $up_gambar['uploads']['file_name'],
                );
                $this->m_shop->edit($data);
                $this->session->set_flashdata('message', 'Film Berhasil Di Tambahkan');
                redirect('shop');
            }
            //Tampa Mengganti Gambar
            $data = array(
                    'id_barang' => $id_barang,
                    'nama' => $this->input->post('nama'),
                    'harga' => $this->input->post('harga'),
                );
                $this->m_shop->edit($data);
                $this->session->set_flashdata('message', 'Film Berhasil Di Tambahkan');
                redirect('shop');

        } else {
            //$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
            $data ['judul']= 'Halaman Administrator';
            $data['shop'] = $this->m_shop->get_data($id_barang);
            //$data['genre'] = $this->m_genre->get_genre();
            //$data['type'] = $this->m_type->get_type();

            $this->load->view('admin/header',$data);
            $this->load->view('admin/admin_shop_edit',$data);
            $this->load->view('admin/sidenav',$data);
            $this->load->view('admin/footer');
        }
    }

    //Delete one item
    public function delete( $id_barang = NULL )
    {
        //hapus gambar
        $barang = $this->m_barang->get_data($id_barang);
        if ($barang->gambar != "") {
            unlink('./assets1/gambar/'. $barang->gambar);
        }
                
        $data = array('id_barang' => $id_barang );
        $this->m_barang->delete($data);
        $this->session->set_flashdata('message', 'Data Berhasil Di Hapus');
         redirect('shop');
    }
}

/* End of file Controllername.php */



?>