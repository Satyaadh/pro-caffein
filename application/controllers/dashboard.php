<?php

class Dashboard extends CI_Controller{

    public function __construct(){
        parent::__construct();

     //   if($this->session->userdata('role_id') !='2')
     //   {
     //       $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
     //       Anda Belum Login!!!
     //       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    //    </div>');
    //    redirect('auth/login');
    //    }
    }

    public function tambah_ke_keranjang()
    {
        $redirect_page=$this->input->post('redirect_page');


        $data = array(
            'id'      => $this->input->post('id'),
            'qty'     => $this->input->post('qty'),
            'price'   => $this->input->post('price'),
            'name'    =>$this->input->post('name'),
    );
    
    $this->cart->insert($data);
    redirect($redirect_page,"refresh");
    }

    public function detail_keranjang()
    {
        $this->load->view('customer/header',$data);
        $this->load->view('keranjang');
        $this->load->view('customer/footer');
    }

    public function hapus_keranjang()
    {
        $this->cart->destroy();
        redirect('welcome');
    }

    public function pembayaran()
    {
        $this->load->view('customer/header',$data);
        $this->load->view('pembayaran');
        $this->load->view('customer/footer');
    }

    public function proses_pesanan()
    {
        $this->cart->destroy();
        $this->load->view('customer/header',$data);
        $this->load->view('proses_pesanan');
        $this->load->view('customer/footer');
    }
}