<div class="main-panel">
    <div class="content">
        <div class="container-fluid">
            <h4 class="page-title">Add Shop</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header ">
                            <h4 class="card-title">Tambah shop</h4>
                        </div>


                        <div class="card-body">
                            <?php 
                            echo validation_errors('<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h5><i class="icon fas fa-ban"></i>','</h5></div>');
                
                if (isset($error_up)) {
                    echo '<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h5><i class="icon fas fa-ban"></i>'.$error_up.'</h5></div>';
                };

                        echo form_open_multipart('shop/add') ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="squareInput">Nama</label>
                                        <input name="nama" type="text" class="form-control input-square"
                                            id="squareInput" placeholder="name" value="<?= set_value('nama')  ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="squareInput">Harga</label>
                                        <input name="harga" type="text" class="form-control input-square"
                                            id="squareInput" placeholder="price" value="<?= set_value('harga')  ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Gambar Barang</label>
                                <input type="file" name="gambar" class="form-control" id="preview_gambar" required>
                            </div>

                            <div class="form-group">
                                <img src="" id="gambar_load" class="img-fluid">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-md">Simpan</button>
                                <a href="<?= base_url('shop') ?>" class="btn btn-danger btn-md">Kembali</a>
                            </div>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>