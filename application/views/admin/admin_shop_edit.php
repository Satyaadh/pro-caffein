<div class="main-panel">
    <div class="content">
        <div class="container-fluid">
            <h4 class="page-title">Shop</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header ">
                            <h4 class="card-title">Edit barang</h4>
                        </div>


                        <div class="card-body">
                            <?php 
                            echo validation_errors('<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h5><i class="icon fas fa-ban"></i>','</h5></div>');
                
                if (isset($error_up)) {
                    echo '<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h5><i class="icon fas fa-ban"></i>'.$error_up.'</h5></div>';
                };
                        echo form_open_multipart('shop/edit/'. $shop->id_barang ) ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="squareInput">Nama</label>
                                        <input name="nama" type="text" class="form-control input-square"
                                            id="squareInput" placeholder="nama" value="<?= $shop->nama ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="squareInput">harga</label>
                                        <input name="harga" type="text" class="form-control input-square"
                                            id="squareInput" placeholder="price" value="<?= $shop->harga ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Gambar Barang</label>
                                <input type="file" name="gambar" class="form-control" id="preview_gambar">
                            </div>

                            <div class="form-group">
                                <img src="<?= base_url('assets1/gambar/'. $shop->gambar)?>" id="gambar_load"
                                    class="img-fluid">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-md">Simpan</button>
                                <a href="<?= base_url('shop') ?>" class="btn btn-danger btn-md">Kembali</a>
                            </div>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>