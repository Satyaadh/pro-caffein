<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Admin</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<link rel="stylesheet" href="<?= base_url()?>assets1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
	<link rel="stylesheet" href="<?= base_url()?>assets1/css/ready.css">
	<link rel="stylesheet" href="<?= base_url()?>assets1/css/demo.css">
</head>
<body>
	<div class="wrapper">
		<div class="main-header">

		<div class="logo-header">
			    <a href="index.html" class="logo">
			        Administrator
			    </a>
			    <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse"
			        data-target="collapse" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle navigation">
			        <span class="navbar-toggler-icon"></span>
			    </button>
			    <button class="topbar-toggler more"><i class="fas fa-ellipsis-v"></i></button>
			</div>
			<nav class="navbar navbar-header navbar-expand-lg">
			    <div class="container-fluid">

			        </ul>
			    </div>
			</nav>
			</div>