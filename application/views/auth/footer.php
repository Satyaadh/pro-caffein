    <!-- Js Plugins -->
    <script src="<?= base_url()?>/assets/auth/js/jquery-3.3.1.min.js"></script>
    <script src="<?= base_url()?>/assets/auth/js/bootstrap.min.js"></script>
    <script src="<?= base_url()?>/assets/auth/js/player.js"></script>
    <script src="<?= base_url()?>/assets/auth/js/jquery.nice-select.min.js"></script>
    <script src="<?= base_url()?>/assets/auth/js/mixitup.min.js"></script>
    <script src="<?= base_url()?>/assets/auth/js/jquery.slicknav.js"></script>
    <script src="<?= base_url()?>/assets/auth/js/owl.carousel.min.js"></script>
    <script src="<?= base_url()?>/assets/auth/js/main.js"></script>


</body>

</html>