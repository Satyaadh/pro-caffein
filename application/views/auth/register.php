
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>


    <!-- Normal Breadcrumb Begin -->
    <section class="normal-breadcrumb set-bg" data-setbg="img/normal-breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="normal__breadcrumb__text">
                        <h2>Register</h2>
                        <p>Welcome to Admin Pro Caffein.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Normal Breadcrumb End -->

    <!-- Login Section Begin -->
    <section class="login spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="login__form">
                        <h3>Register</h3>
                        <form class="user" method="POST" action="<?= base_url('auth/register')?>">
                            <div class="input__item">
                                <input type="text" placeholder="Nama" name="name" value="<?= set_value('name');?>">
                                <?php echo form_error('name','<small class="text-danger pl-3">','</small>') ?>
                                <span class="icon_mail"></span>
                            </div>
                            <div class="input__item">
                                <input type="text" placeholder="Email address"  name="email" value="<?= set_value('email');?>">
                                <?php echo form_error('email','<small class="text-danger pl-3">','</small>') ?>
                                <span class="icon_mail"></span>
                            </div>
                            <div class="input__item">
                                <input type="text" placeholder="No.Telpon" name="no_telp" value="<?= set_value('no_telp');?>">
                                <?php echo form_error('no_telp','<small class="text-danger pl-3">','</small>') ?>
                                <span class="icon_mail"></span>
                            </div>
                            <div class="input__item">
                                <input type="text" placeholder="Password" name="password">
                                <?php echo form_error('password1','<small class="text-danger pl-3">','</small>') ?>
                                <span class="icon_lock"></span>
                            </div>
                            <button type="submit" class="site-btn">Register</button>
                        </form>
                       
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="login__register">
                        <h3>Have Account?</h3>
                        <a href="<?= base_url()?>" class="primary-btn">Login Now</a>
                    </div>
                </div>
            </div>
    </section>