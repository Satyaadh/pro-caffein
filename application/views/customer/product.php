      <!-- product section -->
      <section class="product_section layout_padding">
         <div class="container">
            <div class="heading_container heading_center">
               <h2>
                  Product
               </h2>
            </div>
            <div class="row">
               <?php foreach ($product as $p => $value) { ?>
               <div class="col-sm-6 col-md-4 col-lg-3">
                  <?php
                  echo form_open('keranjang/tambah_ke_keranjang');
                  echo form_hidden('id', $value->id_barang);
                  echo form_hidden('qty', 1);
                  echo form_hidden('name', $value->nama);
                  echo form_hidden('price', $value->harga);
                  echo form_hidden('redirect_page', str_replace('index.php/','',current_url()));
                  ?>
                  <div class="box">
                     <div class="option_container">
                        <div class="options">
                           <button type="submit" class="option1">
                           Add To Cart
                           </button>
                           <button type="button" class="option2">
                           Buy Now
                           </button>
                        </div>
                     </div>
                     <div class="img-box">
                        <img src="<?= base_url('assets1/gambar/'. $value->gambar)?>" class="img-fluid">
                     </div>
                     <div class="detail-box">
                        <h5>
                           <?= $value->nama ?>
                        </h5>
                        <h6>
                           <?=  $value->harga?>
                        </h6>
                     </div>
                  </div>
                  <?php
                  echo form_close();
                  ?>
               </div>
               <?php } ?>
            </div>
                     
            <div class="btn-box">
               <a href="">
               View All products
               </a>
            </div>
         </div>
      </section>