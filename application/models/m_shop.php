<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class m_shop extends CI_Model {

    public function get_shop(){
        $this->db->select('*');
        $this->db->from('barang');
        return $this->db->get()->result();
    }

    public function get_data($id_barang){
        $this->db->select('*');
        $this->db->from('barang');
      //  $this->db->join('type', 'type.id_type = film.id_type', 'left');
       // $this->db->join('genre', 'genre.id_genre = film.id_genre', 'left');
        $this->db->where('id_barang', $id_barang);
        
        return $this->db->get()->row();
    }

    public function add($data){
        $this->db->insert('barang', $data);
    }

    public function edit($data){
        $this->db->where('id_barang', $data['id_barang']);
        $this->db->update('barang', $data);
    }

    public function delete($data){
        $this->db->where('id_barang', $data['id_barang']);
        $this->db->delete('barang', $data);
    }

}

/* End of file ModelName.php */


?>